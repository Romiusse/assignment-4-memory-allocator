#include "mem_internals.h"
#include "mem.h"
#include <stdio.h>

struct block_header* get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void memory_alloc_test() {
    
    printf("memory_alloc_test\n");
    void *heap = heap_init(0);
    if (heap == NULL){
        printf("Error memory_alloc_test\n");
        return;
    }
    debug_heap(stdout, heap);
    printf("\t_malloc\n");
    void *m = _malloc(128);
    if (m == NULL){
        printf("Error memory_alloc_test\n");
        return;
    }
    debug_heap(stdout, heap);
    heap_term();
    printf("OK memory_alloc_test\n");
    
    
}

void free_one_block() {
    printf("free_one_block\n");
    void *heap = heap_init(0);
    if (heap == NULL){
        printf("Error free_one_block\n");
        return;
    }
    debug_heap(stdout, heap);
    
    printf("\t_malloc 1\n");
    void *m1 = _malloc(128);
    if (m1 == NULL){
        printf("Error free_one_block\n");
        return;
    }
    
    printf("\t_malloc 2\n");
    void *m2 = _malloc(128);
    if (m2 == NULL){
        printf("Error free_one_block\n");
        return;
    }
    
    debug_heap(stdout, heap);
    printf("\t_free 2\n");
    _free(m2);
    
    struct block_header* h1 = get_header(m1);
    struct block_header* h2 = get_header(m2);
    
    if (h1->next != h2 || h2->is_free == false) {
        printf("Error free_one_block\n");
    }
    
    _free(m1);
    debug_heap(stdout, heap);
    heap_term();
    printf("OK free_one_block\n");
}

void free_two_blocks() {
    printf("free_two_blocks\n");
    void *heap = heap_init(0);
    if (heap == NULL){
        printf("Error free_two_blocks\n");
        return;
    }
    debug_heap(stdout, heap);
    
    printf("\t_malloc 1\n");
    void *m1 = _malloc(128);
    if (m1 == NULL){
        printf("Error free_two_blocks\n");
        return;
    }
    
    printf("\t_malloc 2\n");
    void *m2 = _malloc(128);
    if (m2 == NULL){
        printf("Error free_two_blocks\n");
        return;
    }
    
    printf("\t_malloc 3\n");
    void *m3 = _malloc(128);
    if (m3 == NULL){
        printf("Error free_two_blocks\n");
        return;
    }
    
    debug_heap(stdout, heap);
    printf("\t_free 3\n");
    _free(m3);
    debug_heap(stdout, heap);
    
    struct block_header* h1 = get_header(m1);
    struct block_header* h2 = get_header(m2);
    struct block_header* h3 = get_header(m3);
    
    if (h2->next != h3 || h3->is_free == false) {
        printf("Error free_two_blocks\n");
    }
    
    printf("\t_free 2\n");
    _free(m2);
    debug_heap(stdout, heap);
    
    if (h1->next != h2 || h2->is_free == false) {
        printf("Error free_two_blocks\n");
    }
    
    _free(m1);
    debug_heap(stdout, heap);
    heap_term();
    printf("OK free_two_blocks\n");
}

void extends_region_test() {
    printf("extends_region_test\n");
    void *heap = heap_init(0);
    if (heap == NULL){
        printf("Error extends_region_test\n");
        return;
    }
    
    debug_heap(stdout, heap);
    
    printf("\t_malloc 1\n");
    void *m1 = _malloc(4096);
    if (m1 == NULL){
        printf("Error extends_region_test\n");
        return;
    }
    
    debug_heap(stdout, heap);
    
    printf("\t_malloc 2\n");
    void *m2 = _malloc(4096 * 3);
    if (m2 == NULL){
        printf("Error extends_region_test\n");
        return;
    }
    
    debug_heap(stdout, heap);
    _free(m2);
    _free(m1);
    heap_term();
    printf("OK extends_region_test\n");
    
}

void extends_region_diff_test() {
    printf("extends_region_diff_test\n");
    void *heap = mmap((void *) HEAP_START, 10, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);
    if (heap == MAP_FAILED) {
        printf("Error extends_region_diff_test\n");
        return;
    }
    void *m = _malloc(128);
    if (m == NULL) {
        printf("Error extends_region_diff_test\n");
        return;
    }
    if (heap == m){
        printf("Error extends_region_diff_test\n");
        return;
    }
    printf("OK extends_region_diff_test\n");
}


int main()
{
    memory_alloc_test();
    free_one_block();
    free_two_blocks();
    extends_region_test();
    extends_region_diff_test();
    return 0;
}
